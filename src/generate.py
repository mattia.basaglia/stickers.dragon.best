import io
import json
import gzip
import shutil
import base64
import pathlib
import argparse

import telethon
import telethon.types
import telethon.functions
from telethon.sync import TelegramClient

import cv2
import jinja2
import lottie
import PIL.Image

src_path = pathlib.Path(__file__).absolute().parent
root_path = src_path.parent

parser = argparse.ArgumentParser()
parser.add_argument("--api-id")
parser.add_argument("--api-hash")
parser.add_argument("--session", default="stickers.dragon.best")
parser.add_argument("--config", default=src_path / "config.json", type=pathlib.Path)
parser.add_argument("--data", default=src_path / "data.json", type=pathlib.Path)
parser.add_argument("--output", default=root_path / "public", type=pathlib.Path)
parser.add_argument("--cached", "-c", action="store_true", help="Use cached data")
parser.add_argument("--clean", action="store_true", help="Removes old data")
parser.add_argument("--download", nargs="+", help="Download images for the given packs (or `all` for everyone)")
parser.add_argument("--html", action="store_true", help="Generate HTML")
parser.add_argument("--templates", default=src_path / "templates", type=pathlib.Path, help="Jinja2 template path")

args = parser.parse_args()
client = TelegramClient(args.session, args.api_id, args.api_hash)


with open(args.config) as f:
    config = json.load(f)

if not args.cached:
    data = config["packs"]

    for pack in data.values():
        if "nsfw" not in pack:
            pack["nsfw"] = False

    print("Fetching data")

    with client:
        for name, pack in data.items():
            result = client(telethon.functions.messages.GetStickerSetRequest(
                telethon.types.InputStickerSetShortName(name),
                hash=0
            ))
            pack["title"] = result.set.title
            pack["short_name"] = result.set.short_name
            pack["url"] = "https://t.me/addstickers/" + result.set.short_name
            if result.set.animated:
                pack["type"] = "animated"
            elif result.set.videos:
                pack["type"] = "video"
            elif result.set.emojis:
                pack["type"] = "emoji"
            elif result.set.masks:
                pack["type"] = "mask"
            else:
                pack["type"] = "static"

            pack["thumb_version"] = result.set.thumb_version

            stickers = []
            pack["stickers"] = stickers
            emoji = {}
            for sticker in result.packs:
                for doc_id in sticker.documents:
                    emoji[doc_id] = sticker.emoticon

            for sticker in result.documents:
                stickers.append({
                    "emoji": emoji[sticker.id],
                    "id": sticker.id,
                    "access_hash": sticker.access_hash,
                    "file_reference": base64.b64encode(sticker.file_reference).decode("ascii"),
                    "mime_type": sticker.mime_type,
                })

        with open(args.data, "w") as f:
            json.dump(data, f, indent=4)

else:
    with open(args.data, "r") as f:
        data = json.load(f)

if args.clean:
    print("Clearing old data")
    shutil.rmtree(args.output)

if args.download:
    if args.download == ["all"]:
        packs = list(data.keys())
    else:
        packs = args.download

    with client:
        for pack_name in packs:
            pack = data[pack_name]
            print("Downloading " + pack_name)
            out_path = args.output / "stickers" / pack_name
            out_path.mkdir(parents=True, exist_ok=True)

            for sticker in pack["stickers"]:
                doc_id = sticker["id"]
                suffix = sticker["mime_type"].rsplit("/", 1)[1]
                if suffix == "x-tgsticker":
                    suffix = "json"

                file_path = out_path / ("%s.%s" % (doc_id, suffix))
                sticker["file"] = file_path
                sticker["suffix"] = suffix
                if not file_path.exists():
                    print("  %s" % sticker["emoji"])
                    doc = telethon.types.InputDocumentFileLocation(
                        doc_id,
                        access_hash=sticker["access_hash"],
                        file_reference=base64.b64decode(sticker["file_reference"]),
                        thumb_size=""
                    )

                    if suffix == "json":
                        file_bytes = client.download_file(doc, bytes)
                        inflated = gzip.decompress(file_bytes)
                        with open(file_path, "wb") as f:
                            f.write(inflated)

                    else:
                        client.download_file(doc, file_path)


            thumb_version = pack["thumb_version"]
            thumb_file = out_path / "thumb.webp"

            print("  Thumbnail")
            if thumb_version is None:
                first = pack["stickers"][0]

                if first["suffix"] == "json":
                    animation = lottie.importers.core.import_lottie(str(first["file"]))
                    out = io.BytesIO()
                    frame = (animation.in_point + animation.out_point) / 2
                    lottie.exporters.cairo.export_png(animation, out, frame)
                    out.seek(0)
                    img = PIL.Image.open(out)
                    img.save(thumb_file)
                elif first["suffix"] == "webp":
                    shutil.copy(first["file"], thumb_file)
                elif first["suffix"] == "webm":
                    vidcap = cv2.VideoCapture(str(first["file"]))
                    success, image = vidcap.read()
                    cv2.imwrite(str(thumb_file), image)
            else:
                input_thumb = telethon.types.InputStickerSetThumb(
                    telethon.types.InputStickerSetShortName(pack_name), thumb_version
                )
                client.download_file(input_thumb, thumb_file)


if args.html:
    environment = jinja2.Environment(loader=jinja2.FileSystemLoader(args.templates))
    pack_template = environment.get_template("pack.html")
    for pack in data.values():
        with open(args.output / (pack["short_name"] + ".html"), "w") as f:
            f.write(pack_template.render(pack=pack))

    list_template = environment.get_template("list.html")
    with open(args.output / "index.html", "w") as f:
        f.write(list_template.render(packs=data.values()))
